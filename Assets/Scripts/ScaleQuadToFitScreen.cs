using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteAlways]
public class ScaleQuadToFitScreen : MonoBehaviour
{
    public enum Geometry { QUAD, PLANE}

    [SerializeField]
    private Geometry geometry = Geometry.QUAD;

    [SerializeField]
    private bool useManualValues = true;

    [Header("Debugging Info (Read Only)")]
    [SerializeField]
    private int screenWidth = 1440;

    [SerializeField]
    private int screenHeight = 1600;

    [SerializeField]
    private float cameraVerticalFOV;

    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        FitQuad();
    }

    private void FitQuad()
    {
        if (mainCamera == null)
        {
            mainCamera = Camera.main;
        }

        //print("Initializing");
        if (!useManualValues)
        {
            cameraVerticalFOV = mainCamera.fieldOfView;
            screenWidth = Screen.width;
            screenHeight = Screen.height;
        }

        // we put the plane at a distance of 1 (localPosition.z)
        float heightInMeters = transform.localPosition.z * Mathf.Tan(cameraVerticalFOV * Mathf.Deg2Rad /2.0f) * 2;
        float widthInMeters = (screenWidth * 1.0f / screenHeight) * heightInMeters;
        if (geometry == Geometry.QUAD)
        {
            transform.localScale = new Vector3(widthInMeters, heightInMeters, 1);
        } else
        {
            // a plane is 10m x 10m and it's rotated
            transform.localScale = new Vector3(widthInMeters / 10.0f, 1, heightInMeters / 10.0f);
        }
    }


#if UNITY_EDITOR
        [CustomEditor(typeof(ScaleQuadToFitScreen))]
        public class ScaleQuadToFitScreenEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                var script = target as ScaleQuadToFitScreen;

                EditorGUILayout.BeginHorizontal();
                // bool running = script.IsRunning;
                if (GUILayout.Button("Fit Quad"))
                {
                    script.FitQuad();
                }
                EditorGUILayout.EndHorizontal();
            }
        }
#endif

}
