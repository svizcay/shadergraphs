using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SetMaterialUVScreenSpacePosition : MonoBehaviour
{
    [SerializeField]
    private Material material;

    [SerializeField]
    private string materialPositionXProperty = "_X_Position";
    [SerializeField]
    private string materialPositionYProperty = "_Y_Position";

    private int blurLevel = 0;

    [SerializeField]
    [Range(0f, 10f)]
    private float scroll = 1f;

    private float scrollUpdateStep = 0.1f;

    private void Awake()
    {
        if (material != null)
        {
            material.SetFloat("_Size", scroll);
        }
    }


    private void Update()
    {
        bool blurDirty = false;
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            blurLevel++;
            if (blurLevel > 2) blurLevel = 2;
            blurDirty = true;
        } else if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            blurLevel--;
            if (blurLevel < 0) blurLevel = 0;
            blurDirty = true;
        }

        float scrollDelta = Input.mouseScrollDelta.y;

        if (scrollDelta != 0f)
        {
            scroll += scrollDelta * scrollUpdateStep;
            scroll = Mathf.Clamp(scroll, 0f, 10f);

            if (material != null)
            {
                material.SetFloat("_Size", scroll);
            }

        }


        // position in pixels; z=0
        Vector3 mousePosition = Input.mousePosition; 

        // normalize to [0, 1]
        Vector2 uvCoordinate = new Vector2(mousePosition.x / Screen.width, mousePosition.y / Screen.height);

        print(uvCoordinate);
        if (material != null)
        {
            // remmeber that the names start with an underscore
            material.SetFloat(materialPositionXProperty, uvCoordinate.x);
            material.SetFloat(materialPositionYProperty, uvCoordinate.y);
            if (blurDirty)
            {
                switch(blurLevel)
                {
                    case 0:
                        material.EnableKeyword("_BLUR_NO_BLUR");
                        material.DisableKeyword("_BLUR_BLUR_X1");
                        material.DisableKeyword("_BLUR_BLUR_X2");
                        break;
                    case 1:
                        material.EnableKeyword("_BLUR_BLUR_X1");
                        material.DisableKeyword("_BLUR_NO_BLUR");
                        material.DisableKeyword("_BLUR_BLUR_X2");
                        break;
                    case 2:
                        material.EnableKeyword("_BLUR_BLUR_X2");
                        material.DisableKeyword("_BLUR_BLUR_X1");
                        material.DisableKeyword("_BLUR_NO_BLUR");
                        break;

                }
                // material.SetInteger("_BLUR", blurLevel);
            }
        }
    }
}
