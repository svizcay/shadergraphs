using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableStereoCamera : MonoBehaviour
{
    // Start is called before the first frame update

    private Camera thisCamera;

    [SerializeField]
    private RenderTexture rt;

    private void Awake()
    {
        thisCamera = GetComponent<Camera>();

        rt.vrUsage = VRTextureUsage.TwoEyes;
    }

    private void Update()
    {
        print(thisCamera.stereoEnabled);

    }
}
