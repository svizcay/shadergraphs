using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMaterialVectorialProperty : SetMaterialProperty
{
    [SerializeField]
    private Vector4 manualValue;

    public void SetValue(Vector4 val)
    {
        if (material == null) return;
        material.SetVector(propertyName, val);
    }

    public void SetValue(Vector3 val)
    {
        if (material == null) return;
        material.SetVector(propertyName, val);
    }

    public void SetValue(Vector2 val)
    {
        if (material == null) return;
        material.SetVector(propertyName, val);
    }

    private void Update()
    {
        if (material == null) return;

        if (applyManualValueOnUpdate)
        {
            SetValue(manualValue);
        }
    }

}
