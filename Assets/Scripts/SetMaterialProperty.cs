using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMaterialProperty : MonoBehaviour
{
    public enum PropertyType { Float, Int, Boolean, Enum }

    [SerializeField]
    protected string propertyName;

    [SerializeField]
    protected Material material;

    [SerializeField]
    protected bool applyManualValueOnUpdate = false;
}
