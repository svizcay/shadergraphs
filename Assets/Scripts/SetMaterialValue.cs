using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SetMaterialValue : MonoBehaviour
{
    [SerializeField]
    private Transform targetObject;

    [SerializeField]
    private string propertyName = "_Gaze_3D_WorldSpace";

    [SerializeField]
    private Material material;

    private void Update()
    {
        if (material == null) return;
        if (targetObject == null) return;

        material.SetVector(propertyName, targetObject.transform.position);
    }
}
