using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMaterialProperties : MonoBehaviour
{
    public enum PropertyType { Float, Int, Boolean, Enum }

    [Serializable]
    public struct Property
    {
        public string name;
        public PropertyType type;
        public bool applyOnUpdate;
        public object value;
    }

    [SerializeField]
    private List<Property> properties;

    [SerializeField]
    private Material material;

    private void Update()
    {
        if (material == null) return;
    }

    private void SetProperty(Property prop)
    {
        switch (prop.type)
        {
            case PropertyType.Float:
                material.SetFloat(prop.name, 0f);
                break;
        }
    }
}
