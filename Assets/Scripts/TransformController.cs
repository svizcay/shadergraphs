using UnityEngine;
using UnityEngine.InputSystem;

public class TransformController : MonoBehaviour
{
    public float linearSpeed = 10f;
    public float angularSpeed = 45f;

    public Key resetTransformKey = Key.R;

    private Vector3 originalPosition; 
    private Vector3 originalRotation; 

    private void Awake()
    {
        originalPosition = transform.localPosition;
        originalRotation = transform.localEulerAngles;
    }
    private void Update()
    {
        Rotate();
        Move();

        bool isShiftKeyPressed = Keyboard.current[Key.LeftShift].isPressed || Keyboard.current[Key.RightShift].isPressed;

        if (Keyboard.current[resetTransformKey].isPressed)
        {
            ResetRotation();
            if (isShiftKeyPressed)
            {
                ResetPosition();
            }
        }
    }

    private void Move()
    {
        // float leftInput = Keyboard.current.leftArrowKey.isPressed ? -1f : 0f;
        // float rightInput = Keyboard.current.rightArrowKey.isPressed ? 1f : 0f;
        float leftInput = Keyboard.current.aKey.isPressed ? -1f : 0f;
        float rightInput = Keyboard.current.dKey.isPressed ? 1f : 0f;
        float horizontalInput = leftInput + rightInput;

        float upInput = Keyboard.current.eKey.isPressed ? 1f : 0f;
        float downInput = Keyboard.current.qKey.isPressed ? -1f : 0f;
        float verticalInput = upInput + downInput;

        float forwardInput = Keyboard.current.wKey.isPressed ? 1f : 0f;
        float backwardInput = Keyboard.current.sKey.isPressed ? -1f : 0f;
        float depthInput = forwardInput + backwardInput;

        Vector3 direction = new Vector3(horizontalInput, verticalInput, depthInput).normalized;
        direction *= Time.deltaTime * linearSpeed;

        transform.Translate(direction); //default behaviour is to translate relative to the local space
    }

    private void Rotate()
    {
        float yawLeft = Keyboard.current.jKey.isPressed ? -1f : 0f;
        float yawRight = Keyboard.current.lKey.isPressed ? 1f : 0f;
        float yawInput = yawLeft + yawRight;

        float pitchUp = Keyboard.current.kKey.isPressed ? 1f : 0f;
        float pitchDown = Keyboard.current.iKey.isPressed ? -1f : 0f;
        float pitchInput = pitchDown + pitchUp;

        float rollLeft = Keyboard.current.uKey.isPressed ? 1f : 0f;
        float rollRight = Keyboard.current.oKey.isPressed ? -1f : 0f;
        float rollInput = rollLeft + rollRight;

        Vector3 direction = new Vector3(pitchInput, yawInput, rollInput).normalized;
        direction *= Time.deltaTime * angularSpeed;

        transform.Rotate(direction); //default behaviour is to translate relative to the local space
    }

    private void ResetPosition()
    {
        transform.localPosition = originalPosition;
        ResetRotation();
    }

    private void ResetRotation()
    {
        transform.localEulerAngles = originalRotation;
    }
}
