using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CustomRenderPassFeature : ScriptableRendererFeature
{
[System.Serializable]
    public class BlitOpaqueToRenderTextureSettings {
        public RenderPassEvent renderPassEvent = RenderPassEvent.AfterRenderingOpaques;
        public RenderTexture targetRenderTexture;
        public Material blitMaterial;
        public BuiltinRenderTextureType inputRenderTexture;
    }

    public BlitOpaqueToRenderTextureSettings settings = new BlitOpaqueToRenderTextureSettings();

    class CustomRenderPass : ScriptableRenderPass
    {
        private RenderTexture targetRenderTexture;

        private Material blitMaterial;

        private BuiltinRenderTextureType inputRenderTexture;

        public CustomRenderPass(RenderTexture targetRenderTexture, Material blitMaterial, BuiltinRenderTextureType inputRenderTexture)
        {
            this.targetRenderTexture = targetRenderTexture;
            this.blitMaterial = blitMaterial;
            this.inputRenderTexture = inputRenderTexture;
        }

        // This method is called before executing the render pass.
        // It can be used to configure render targets and their clear state. Also to create temporary render target textures.
        // When empty this render pass will render to the active camera render target.
        // You should never call CommandBuffer.SetRenderTarget. Instead call <c>ConfigureTarget</c> and <c>ConfigureClear</c>.
        // The render pipeline will ensure target setup and clearing happens in a performant manner.
        public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
        {
        }

        // Here you can implement the rendering logic.
        // Use <c>ScriptableRenderContext</c> to issue drawing commands or execute command buffers
        // https://docs.unity3d.com/ScriptReference/Rendering.ScriptableRenderContext.html
        // You don't have to call ScriptableRenderContext.submit, the render pipeline will call it at specific points in the pipeline.
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get("MyCommandBuffer");

            // Set up rendering to the target Render Texture
            // cmd.SetRenderTarget(settings.targetRenderTexture);
            // cmd.SetRenderTarget(targetRenderTexture);

            // Blit the opaque texture
            // cmd.Blit(BuiltinRenderTextureType.CameraTarget, settings.targetRenderTexture, blitMaterial);
            // cmd.Blit(BuiltinRenderTextureType.CameraTarget, targetRenderTexture);
            //cmd.Blit(BuiltinRenderTextureType.CameraTarget, targetRenderTexture, blitMaterial);
            // cmd.Blit(BuiltinRenderTextureType.CameraTarget, targetRenderTexture);
            // cmd.Blit(BuiltinRenderTextureType.CameraTarget, targetRenderTexture);
            cmd.Blit(BuiltinRenderTextureType.RenderTexture, targetRenderTexture, blitMaterial);

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        // Cleanup any allocated resources that were created during the execution of this render pass.
        public override void OnCameraCleanup(CommandBuffer cmd)
        {
        }
    }

    CustomRenderPass m_ScriptablePass;

    /// <inheritdoc/>
    public override void Create()
    {
        //settings.targetRenderTexture.format = RenderTextureFormat.Default;
        m_ScriptablePass = new CustomRenderPass(settings.targetRenderTexture, settings.blitMaterial, settings.inputRenderTexture);

        // Configures where the render pass should be injected.
        m_ScriptablePass.renderPassEvent = settings.renderPassEvent;

        // Replace with your desired shader
        //m_ScriptablePass.blitMaterial = new Material(Shader.Find("Standard")); 
        // m_ScriptablePass.targetRenderTexture = settings.targetRenderTexture;
    }

    // Here you can inject one or multiple render passes in the renderer.
    // This method is called when setting up the renderer once per-camera.
    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        renderer.EnqueuePass(m_ScriptablePass);
    }
}


