#ifndef MY_CUSTOM_NODE_INCLUDED
#define MY_CUSTOM_NODE_INCLUDED

//#pragma exclude_renderers d3d11

void Blur_float(int n, float4 UVs, float2 TextureResolution, UnityTexture2D Texture, UnitySamplerState Sampler, out float4 Output)
{
    int nr_invalid_uvs = 0;
    Output = float4(0, 0, 0, 0);
    [loop]
    for (int i = -n; i <= n; i++)
    {
        [loop]
        for (int j = -n; j <= n; j++)
        {
            float2 pixel_offset = float2(i, j);
            float2 sampling_uv = pixel_offset * (1 / TextureResolution) + UVs.xy;
            if (sampling_uv.x > 1 || sampling_uv.x < 0 || sampling_uv.y > 1 || sampling_uv.y < 0)
            {
                nr_invalid_uvs++;
            }
            else
            {
                // Output = Output + SAMPLE_TEXTURE2D(Texture, Sampler, sampling_uv);
                Output = Output + SAMPLE_TEXTURE2D_LOD(Texture, Sampler, sampling_uv, 0);
            }
        }
    }
    Output = Output / ((2 * n + 1) * (2 * n + 1) - nr_invalid_uvs);
    Output.a = 1;
    //Output = tex2D(Texture, UVs.xy);
}

void Blur_half(int n, float4 UVs, float2 TextureResolution, UnityTexture2D Texture, UnitySamplerState Sampler, out float4 Output)
{
    int nr_invalid_uvs = 0;
    Output = float4(0, 0, 0, 0);
    [loop]
    for (int i = -n; i <= n; i++)
    {
        [loop]
        for (int j = -n; j <= n; j++)
        {
            float2 pixel_offset = float2(i, j);
            float2 sampling_uv = pixel_offset * (1 / TextureResolution) + UVs.xy;
            if (sampling_uv.x > 1 || sampling_uv.x < 0 || sampling_uv.y > 1 || sampling_uv.y < 0)
            {
                nr_invalid_uvs++;
            }
            else
            {
                //Output = Output + SAMPLE_TEXTURE2D(Texture, Sampler, sampling_uv);
                Output = Output + SAMPLE_TEXTURE2D_LOD(Texture, Sampler, sampling_uv, 0);
            }
        }
    }
    Output = Output / ((2 * n + 1) * (2 * n + 1) - nr_invalid_uvs);
    Output.a = 1;
    //Output = tex2D(Texture, UVs.xy);
}

#endif
