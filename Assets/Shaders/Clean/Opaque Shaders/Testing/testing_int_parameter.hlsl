#ifndef Testing_Int_Parameter_INCLUDED
#define Testing_Int_Parameter_INCLUDED

//#pragma exclude_renderers d3d11

void test_int_parameter_float(int K, int N, out float4 Output)
{
    float gray_intensity = K * 1.0 / N;
    Output = float4(gray_intensity, gray_intensity, gray_intensity, 1);
}

void test_int_parameter_half(int K, int N, out float4 Output)
{
    float gray_intensity = K * 1.0 / N;
    Output = float4(gray_intensity, gray_intensity, gray_intensity, 1);
}

void test_float_parameter_float(float K, float N, out float4 Output)
{
    float gray_intensity = K / N;
    Output = float4(gray_intensity, gray_intensity, gray_intensity, 1);
}

void test_float_parameter_half(float K, float N, out float4 Output)
{
    float gray_intensity = K / N;
    Output = float4(gray_intensity, gray_intensity, gray_intensity, 1);
}
#endif
