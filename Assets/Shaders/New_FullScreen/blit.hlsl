#ifndef MY_BLIT
#define MY_BLIT

// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Packing.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/NormalSurfaceGradient.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/EntityLighting.hlsl"
// #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariables.hlsl"
// #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
// #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"

float4 _BlitTexture_TexelSize;

void My_Blit_float(out float4 Output)
{
    Output = float4(0, 0, 0, 0);
}

void My_Blit_half(out float4 Output)
{
    uint2 pixelCoords = uint2(uv * _ScreenSize.xy);
    //return LOAD_TEXTURE2D_X_LOD(_BlitTexture, pixelCoords, 0);
    Output = float4(0, 0, 0, 0);
    // Output = LOAD_TEXTURE2D_X_LOD(_BlitTexture, pixelCoords, 0);
}

#endif
